Name: softhsm
Version: 2.6.1
Release: 5
Summary: Software version of a PKCS#11 Hardware Security Module
License: BSD
URL: http://www.opendnssec.org/
Source: http://dist.opendnssec.org/source/%{name}-%{version}.tar.gz
Source1: http://dist.opendnssec.org/source/%{name}-%{version}.tar.gz.sig

BuildRequires: openssl-devel >= 1.0.1k-6, sqlite-devel >= 3.4.2, cppunit-devel
BuildRequires: gcc-c++, pkgconfig, p11-kit-devel, nss-devel

Requires: shadow-utils, p11-kit, nss-tools, openssl-libs >= 1.0.1k-6

%description
OpenDNSSEC is providing a software implementation of a generic
cryptographic device with a PKCS#11 interface, the SoftHSM. SoftHSM is
designed to meet the requirements of OpenDNSSEC, but can also work together
with other cryptographic products because of the PKCS#11 interface.

%package devel
Summary: Development package of softhsm that includes the header files
Requires: %{name} = %{version}-%{release}, openssl-devel, sqlite-devel
BuildRequires: autoconf, libtool, automake

%description devel
The devel package contains the libsofthsm include files

%package_help

%prep
%autosetup  -n %{name}-%{version}

sed -i "s:full_libdir/softhsm:full_libdir:g" configure
sed -i 's:^full_libdir=":#full_libdir=":g' configure.ac
sed -i "s:libdir)/@PACKAGE@:libdir):" Makefile.in

%build
autoreconf -fi
%configure --libdir=%{_libdir}/pkcs11 --with-openssl=%{_prefix} --enable-ecc --disable-gost \
           --with-migrate --enable-visibility --with-p11-kit=%{_datadir}/p11-kit/modules/

make %{?_smp_mflags}

%check
#make check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install
mkdir -p %{buildroot}%{_includedir}/softhsm
cp src/lib/*.h %{buildroot}%{_includedir}/softhsm
mkdir -p %{buildroot}/%{_sharedstatedir}/softhsm/tokens
mkdir -p %{buildroot}/%{_libdir}/softhsm/

ln -s ../pkcs11/libsofthsm2.so %{buildroot}/%{_libdir}/softhsm/libsofthsm.so
# Link libsofthsm2.so to /usr/lib64 because NSS needs it to
# be in the standard library search path.
( cd  %{buildroot}/%{_libdir} ; ln -s pkcs11/libsofthsm2.so)

rm %{buildroot}/%{_sysconfdir}/softhsm2.conf.sample
rm -f %{buildroot}/%{_libdir}/pkcs11/*a

%files
%config(noreplace) %{_sysconfdir}/softhsm2.conf
%{_bindir}/*
%dir %{_libdir}/softhsm
%{_libdir}/softhsm/libsofthsm.so
%{_libdir}/pkcs11/libsofthsm2.so
%{_libdir}/libsofthsm2.so

%attr(0664,root,root) %{_datadir}/p11-kit/modules/softhsm2.module
%attr(0750,ods,ods) %dir %{_sharedstatedir}/softhsm
%attr(1770,ods,ods) %dir %{_sharedstatedir}/softhsm/tokens


%doc LICENSE README.md NEWS

%files devel
%attr(0755,root,root) %dir %{_includedir}/softhsm
%{_includedir}/softhsm/*.h

%files help
%{_mandir}/*/*

%pre
getent group ods >/dev/null || groupadd -r ods
getent passwd ods >/dev/null || \
    useradd -r -g ods -d %{_sharedstatedir}/softhsm -s /sbin/nologin \
    -c "softhsm private keys owner" ods
exit 0

%post

%triggerpostun -- softhsm < 2.0.0
if [ -f /var/softhsm/slot0.db ]; then
      runuser -g ods ods -c 'softhsm2-migrate --db /var/softhsm/slot0.db --pin 1234 --slot 0' || :
fi

%changelog
* Fri Feb 7 2025 xuce<xuce10@h-partners.com> - 2.6.1-5
- Rebuild the configure file. 

* Thu Feb 2 2023 zhengxiaoxiao<zhengxiaoxiao2@huawei.com> - 2.6.1-4
- Disable the make check to temporarily avoid the build failure issue

* Thu Oct 20 2022 zhengxiaoxiao<zhengxiaoxiao2@huawei.com> 2.6.1-3
- update release

* Fri Dec 25 2020 panxiaohe<panxiaohe@huawei.com> 2.6.1-2
- fix relative path of libsofthsm2.so to ensure file exists

* Fri Jul 24 2020 linwei<linwei54@huawei.com> 2.6.1-1
- update softhsm to 2.6.1

* Fri Feb 14 2020  openEuler Buildteam <buildteam@openeuler.org> 2.5.0-3
- make check after build

* Tue Dec 24 2019  openEuler Buildteam <buildteam@openeuler.org> 2.5.0-2
- Spec format

* Tue Sep 3 2019  openEuler Buildteam <buildteam@openeuler.org> 2.5.0-1
- Package init

